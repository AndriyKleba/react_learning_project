import React from "react";
import classes from './Header.module.css';

const avatar = 'https://placeit-assets1.s3-accelerate.amazonaws.com/custom-pages/2019-logo-maker/Esports-Logo-Maker.png';

const Header = () => {
    return (
        <header className={classes.header}>
            <img src={avatar}/>
        </header>
    )
};

export default Header;
