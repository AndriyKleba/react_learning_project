import React from "react";
import classes from './Dialogs.module.css'
import DialogItem from "./DialogItem/DialogItem";
import Message from "./Message/Message";
import {sendMessageCreator, updateNewMessageBodyCreator} from "../../redux/profilePageReducer";
import Dialogs from "./Dialogs";


const DialogsContainer = (props) => {

    let state = props.store.getState().profilePage;

    let onSendMessageClick = () => {
        props.store.dispatch(sendMessageCreator());
    };

    let updateNewMessageBody = (body) => {
        props.store.dispatch(updateNewMessageBodyCreator(body));
    };

    return <Dialogs
        updateNewMessageBody={updateNewMessageBody}
        sendMessage={onSendMessageClick}
        profilePage={state}
    />
};

export default DialogsContainer;
