import React from "react";
import classes from './MyPosts.module.css'
import Post from "./Post/Post";
import {addPostActionCreator, updatePostActionCreator} from "../../../redux/listPostsReducer";



const MyPosts = (props) => {

    let postsElements = props.posts.map((post, index) =>
        <Post message={post.message} like={post.like} id={post.id} key={post.id}/>
    );

    const newPostElement = React.createRef();

    let addPost = () => {
        props.addPost();
    };

    let onPostChange = () => {
        let text = newPostElement.current.value;
        props.updateNewPostText(text);

    };

    return (
        <div>
            <div className={classes.post}><h3>My post</h3></div>
            <div>
                <div>
                    <textarea
                        onChange={onPostChange}
                        ref={newPostElement}
                        value={props.newPostText}/>
                </div>
                <div>
                    <button onClick={addPost}>ADD Post</button>
                </div>
            </div>
            <div className={classes.post}>
                {
                    postsElements
                }
            </div>
        </div>
    )
};

export default MyPosts;
