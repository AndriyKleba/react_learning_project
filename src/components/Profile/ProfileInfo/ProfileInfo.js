import React from "react";
import classes from './ProfileInfo.module.css'

const font = 'https://wallpapershome.com/images/pages/pic_h/21486.jpg';

const ProfileInfo = (props) => {
    return (
        <div>
            <div><img src={font} className={classes.font}/></div>
            <div className={classes.descriptionBlock}> ava + description</div>
        </div>
    )
};

export default ProfileInfo;
