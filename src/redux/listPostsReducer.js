const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';

let initialState = {
        posts: [
            {message: 'Hello, how are you?', like: 12, id: 1},
            {message: 'Will you drink beer?', like: 26, id: 2},
        ],
        newPostText: '',
};


const listPostsReducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_POST:
            let newPost = {
                id: 5,
                like: 24,
                message: state.newPostText
            };

            state.posts.push(newPost);
            state.newPostText = '';
            return state;
        case UPDATE_NEW_POST_TEXT:
            state.newPostText = action.text;
            return state;
        default:
            return state;
    }
};

export const addPostActionCreator = () => ({type: ADD_POST});
export const updatePostActionCreator = (text) =>
    ({type: UPDATE_NEW_POST_TEXT, text: text});

export default listPostsReducer;
