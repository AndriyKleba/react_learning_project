const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE_NEW_MESSAGE_TEXT';
const SEND_MESSAGE_TEXT = 'SEND_MESSAGE_TEXT';

let initialState = {
    dialogs: [
        {id: 1, name: 'Victor'},
        {id: 2, name: 'Anna'},
        {id: 3, name: 'Lena'},
        {id: 4, name: 'Andrii'},
        {id: 5, name: 'Oliver'},
        {id: 6, name: 'Dmitro'},
    ],
    messages: [
        {message: 'How are you?', id: 1},
        {message: 'Hi', id: 2},
        {message: 'Show your phone!', id: 3},
    ],
    newMessageBody: ''
};


const profilePageReducer = (state = initialState, action) => {

    switch (action.type) {

        case UPDATE_NEW_MESSAGE_TEXT:
            state.newMessageBody = action.body;
            return state;

        case SEND_MESSAGE_TEXT:
            const body = state.newMessageBody;
            state.newMessageBody = '';
            state.messages.push({message: body, id: 4});
            return state;

        default:
            return state;
    }
};

export const sendMessageCreator = () => ({type: SEND_MESSAGE_TEXT});

export const updateNewMessageBodyCreator = (body) =>
    ({UPDATE_NEW_MESSAGE_TEXT, body: body});

export default profilePageReducer;
