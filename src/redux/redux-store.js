import {combineReducers, createStore} from "redux";
import profilePageReducer from "./profilePageReducer";
import listPostsReducer from "./listPostsReducer";
import sidebarReducer from "./sidebarReducer";

const reducers = combineReducers({
    profilePage: profilePageReducer,
    listPosts: listPostsReducer,
    sidebar: sidebarReducer
});

const store = createStore(reducers);

export default store;

