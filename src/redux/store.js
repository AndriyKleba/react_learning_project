import listPostsReducer from "./listPostsReducer";
import profilePageReducer from "./profilePageReducer";
import sidebarReducer from "./sidebarReducer";



let store = {
    _state: {
        listPosts: {
            posts: [
                {message: 'Hello, how are you?', like: 12, id: 1},
                {message: 'Will you drink beer?', like: 26, id: 2},
            ],
            newPostText: '',
        },
        profilePage: {
            dialogs: [
                {id: 1, name: 'Victor'},
                {id: 2, name: 'Anna'},
                {id: 3, name: 'Lena'},
                {id: 4, name: 'Andrii'},
                {id: 5, name: 'Oliver'},
                {id: 6, name: 'Dmitro'},
            ],
            messages: [
                {message: 'How are you?', id: 1},
                {message: 'Hi', id: 2},
                {message: 'Show your phone!', id: 3},
            ],
            newMessageBody: ''
        },
        sidebar: {}
    },
    _callSubscriber() {
        console.log('State')
    },
    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },
    dispatch(action) {

        this._state.listPosts = listPostsReducer(this._state.listPosts, action);
        this._state.profilePage = profilePageReducer(this._state.profilePage, action);
        this._state.sidebar = sidebarReducer(this._state.sidebar, action);

        this._callSubscriber(this._state)
    }
};






export default store;
